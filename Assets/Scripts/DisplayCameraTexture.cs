﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCameraTexture : MonoBehaviour
{
    public RawImage rawimage;
    public Renderer renderer;
    public GameObject catalogUI;
    public static bool isToggled = false;

    private float tlx, tly, brx, bry;
    private int width, height;
    private int delay = 0;

    [DllImport("OpenCVWrapperLibrary", EntryPoint = "DetectChair", CallingConvention = CallingConvention.StdCall)]
    public static extern void DetectChair(long length, int width, int height, byte[] data, out float tlx, out float tly, out float brx, out float bry);

    [DllImport("OpenCVWrapperLibrary", EntryPoint = "LoadClassifier", CallingConvention = CallingConvention.StdCall)]
    public static extern void LoadClassifier();

    // Start is called before the first frame update
    void Start()
    {
        WebCamTexture webcamTexture = new WebCamTexture();
        renderer.material.mainTexture = webcamTexture;
        webcamTexture.Play();
        LoadClassifier();
    }

    private void Update()
    {
        Classify();
    }

    private void Classify()
    {
        Texture2D t = new Texture2D(renderer.material.mainTexture.width, renderer.material.mainTexture.height, TextureFormat.RGB24, false);
        t.hideFlags = HideFlags.HideAndDontSave;
        t.SetPixels((renderer.material.mainTexture as WebCamTexture).GetPixels());
        t.Apply();

        byte[] bytes = t.GetRawTextureData();
        DetectChair(bytes.Length, t.width, t.height, bytes, out tlx, out tly, out brx, out bry);

        width = t.width;
        height = t.height;
        Debug.Log("FaceCoords: (" + tlx + ", " + tly + ", " + brx + ", " + bry + ")");

        renderer.sharedMaterial.SetFloat("CX", 1 - (tlx + brx / 2) / width);
        renderer.sharedMaterial.SetFloat("CY", (tly + bry / 2) / height);
        renderer.sharedMaterial.SetFloat("R", bry / (2 * height));

        Object.Destroy(t);
    }
}
