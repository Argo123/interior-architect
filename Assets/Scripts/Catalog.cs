﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catalog : MonoBehaviour
{
    public static bool isToggled = false;
    public GameObject catalogUI;

    // Start is called before the first frame update
    void Start()
    {
        catalogUI.SetActive(isToggled);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Toggle()
    {
        isToggled = !isToggled;
        catalogUI.SetActive(isToggled);

        Debug.Log("Catalog toggled!", this);
    }
}
