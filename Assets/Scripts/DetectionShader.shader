﻿Shader "Custom/DetectionShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		CX("Center-X", Float) = 30
		CY("Center-Y", Float) = 130
		R("Radius", Float) = 20
		W("Width", Float) = 0.05
		A("AspectRatio", Float) = 1
	}
		SubShader
		{
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				

				struct v2f
				{
					float2 texcoord : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata_base v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.texcoord = v.texcoord;
					return o;
				};

				sampler2D _MainTex;
				float CX;
				float CY;
				float R;
				float T;
				float A;
				float W;
				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, float2 (i.texcoord.x, 1 - i.texcoord.y));
					CX = CX * A;
					float x = i.texcoord.x * A;
					float y = i.texcoord.y;
					float Left = (x - CX) * (x - CX) + (y - CY) * (y - CY);
					float Right = R * R;
					if (Left >= (1 - W) * Right && Left <= (1 + W) * Right)
					{
						col = fixed4(1, 0, 0, 1);
					}
					return col;
				}
				ENDCG
			}
		}
			Fallback "Unlit/Color"
}
