﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using Microsoft.MixedReality.Toolkit.UI;

public class CatalogContent : MonoBehaviour
{
    public GameObject objectTemplate;
    public int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        objectTemplate.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateObject()
    {
        var newObject = GameObject.Instantiate(objectTemplate);
        newObject.SetActive(true);
        newObject.name = newObject.name + "_" + count;
        newObject.AddComponent(typeof(BoundsControl));
        var toolTip = GameObject.Instantiate(GameObject.Find("Simple Line ToolTip"));
        toolTip.GetComponent<ToolTipConnector>().Target = newObject;
        toolTip.GetComponent<ToolTip>().ToolTipText = newObject.name;
        toolTip.transform.SetParent(newObject.transform);
        //Float y should reference collider height
        float y = 2;
        float x = 0;
        toolTip.GetComponent<Transform>().position = newObject.transform.position + new Vector3(x, y, 0);
        toolTip.GetComponent<Transform>().localScale = toolTip.GetComponent<Transform>().localScale * 2;
        count++;

        Debug.Log("Item added!", this);
        //Debug.Log(count);
    }
}
